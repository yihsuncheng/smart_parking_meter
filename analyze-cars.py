#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 28 17:43:58 2018

@author: Ethan Cheng
"""
from modules.yolo3 import carFinder
import argparse

#%%
if 0:
    carFinder.same_car('1538076003.jpg', '1538076191.jpg')

if __name__ == "__main__":
    # Define arguments
    parser = argparse.ArgumentParser(description='Detect the duration of a stationaty car in the target slot.')
    parser.add_argument('--index', type= str, help = 'The index file to be loaded.', required = True)
    parser.add_argument('--start', type= int, help="The starting time stamp.", required = True)
    parser.add_argument('--end', type=int, help="The ending timestamp.", required = True)
    args = parser.parse_args()
    tstamps = []
    try:
       with open(args.index, 'r') as f:
           lines = f.readlines()
    except:
       raise ValueError('Error: index file - {} does not exist !!'.format(args.index))
    for line in lines:
        tstamps += [int(line.split('.')[0])]
    diffs, hasCar, ts, im1Out = carFinder.temporal_diff(args.start, args.end, tstamps)
