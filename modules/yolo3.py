#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 15:51:23 2018

@author: Ethan Cheng
"""
from keras import backend as K
from keras.models import load_model
#from keras.layers import Input
import numpy as np
import  cv2, os
from modules.yolo_model import yolo_eval
import matplotlib.pyplot as plt
from threading import Thread
from moviepy.editor import VideoFileClip
from matplotlib.image import imsave


class videoDownloader(Thread):
    def __init__(self, timestamp, outDir):
        Thread.__init__(self)
        self.outDir = outDir
        self.url = 'https://hiring.verkada.com/video/{}.ts'.format(timestamp)
        self.outFile = '{}/{}.jpg'.format(self.outDir,timestamp)
        self.fail = False
                
    def run(self):
        if not os.path.exists(self.outFile): # donwload only when it does not exist
            print('Downloading ' + self.url)
            try:
                clip = VideoFileClip(self.url)
                plt.imsave(self.outFile, clip.get_frame(0))
            except:
                print('Error: fail to download')
                self.fail = True
            
#%%            
class YOLO(object):
    _defaults = {
        "model_path": 'modules/yolov3.h5',
        "anchors_path": 'modules/yolo_anchors.txt',
        "classes_path": 'modules/coco.names',
        "score" : 0.1,
        "iou" : 0.45,
        "model_image_shape" : [416, 416, 3],
        "gpu_num" : 1,
        "max_boxes" : 20,
        "target": np.int_([[90, 96], [140, 140]]),
        "target_center": np.float_([[98, 113],[126,125]]),
    }

    def __init__(self, **kwargs):
        self.__dict__.update(self._defaults) # set up default values
        self.__dict__.update(kwargs) # and update with user overrides
        self.anchors = self._get_anchors()
        self.class_names = self._get_class()
        self.yolo_model = load_model(self.model_path, compile = False)
        self.num_layers = len(self.yolo_model.outputs)
        self.sess = K.get_session()
        self.input_image_shape = K.placeholder(shape=(2, ))
        # create containers for model eval
        self.boxes, self.scores, self.classes = yolo_eval(
                self.yolo_model.output, 
                self.anchors,
                len(self.class_names),
                self.input_image_shape,
                score_threshold=self.score,
                iou_threshold=self.iou)
        self.features = [self.yolo_model.layers[-6].output,
                         self.yolo_model.layers[-5].output,
                         self.yolo_model.layers[-4].output]
        
    def _get_class(self):
#        classes_path = os.path.expanduser(self.classes_path)
        with open(self.classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
#        anchors_path = os.path.expanduser(self.anchors_path)
        with open(self.anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def normalize_image(self, img):
        '''resize image with preserved aspect ratio'''
        if img.shape[-1] > 3: # get rid of the last channel for png
            img = img[:,:,:3]
        if img.dtype == np.uint8:
            img = img.astype('float32') /255.0 # convert to float
        img_size = np.array(img.shape[:2])
        scale = np.min(np.array(self.model_image_shape[:2])/img_size)
        new_img_size = tuple((img_size * scale).astype(int))
        img_new =  cv2.resize(img, new_img_size[::-1])
        img_model = np.full(self.model_image_shape, 0.5, dtype = img_new.dtype)
        img_model[:new_img_size[0], :new_img_size[1], :] = img_new       
        return img_model
    
    def detect_image(self, img):
        img = self.normalize_image(img)
        out_boxes, out_scores, out_classes, features = self.sess.run(
            [self.boxes, self.scores, self.classes, self.features],
            feed_dict={
                self.yolo_model.input: np.expand_dims(img, 0),  # Add batch dimension.
                self.input_image_shape: self.model_image_shape[:2][::-1],
                K.learning_phase(): 0
            })
        targFeat = self.target_feature(features)
        # convert output images to uint8
        L, H = np.min(img), np.max(img)
        im1 = (255*(img - L)/(H-L)).astype(np.uint8)
        im1 = im1[:312,:,:] #crop back to original size
        return out_boxes, out_scores, out_classes, targFeat, im1
        
    def target_feature(self, features):
        targFeat = []
        for ft_i in features:

            ft = ft_i[0]       
            scale = np.float_(ft.shape[:2])/np.float_(self.model_image_shape[:2])
            bbox = (np.float_(self.target)*scale).round().astype(int)
            targFeat += [ft[bbox[0,1]:bbox[1,1]+1, bbox[0,0]:bbox[1,0]+1,:].flatten()]
        return np.hstack(targFeat)
# ===================== visuzlize ROI ======================================
#             mask = np.zeros(ft.shape[:2], dtype = np.uint8)
#             mask[bbox[0,1]:bbox[1,1]+1, bbox[0,0]:bbox[1,0]+1] = 255
#             plt.figure()
#             plt.imshow(mask)
# ===============================++=========================================
            
    
    def box_overlap_target(self, im1, box):
        mask = np.zeros_like(im1, dtype = int)
        box = np.round(box).astype(int)
        mask[box[0,1]:box[1,1], box[0,0]:box[1,0]] = 1
        I = float(np.sum(mask[self.target[0,1]:self.target[1,1], self.target[0,0]:self.target[1,0]]))
        base = float(np.sum(mask))
        ratio = I/base if base > 0 else 0
        return ratio > 0.75
        
    
    def box_center_target(self, im1, box):
        boxCen = np.mean(box, axis = 0)
        if boxCen[0] < self.target_center[0,0] or boxCen[0] > self.target_center[1,0]:
            return False
        if boxCen[1] < self.target_center[0,1] or boxCen[1] > self.target_center[1,1]:
            return False
        return True
    
    def has_car(self, fname, show = False):
        try:
            img = cv2.imread('data/' + fname)[:,:,::-1]
        except:
            return False, [], []
        out_boxes, out_scores, out_classes, features, im1 = self.detect_image(img)       
        sc = []
        for box, lab, score in zip(out_boxes, out_classes, out_scores):
            if lab != 2:
                continue
            box = np.fliplr(np.reshape(box, (2,2)))
            if self.box_overlap_target(im1, box) and self.box_center_target(im1,box):
                cv2.rectangle(im1,tuple(box[0,:]), tuple(box[1,:]), (0, 255, 0), 1)
                sc += [score]
        if show:
            plt.figure()
            plt.imshow(im1)
        return len(sc) > 0, features, im1


    def same_car(self, fname1, fname2):
        found1, ft1 , im1 = self.has_car(fname1)
        if found1 == False:
            return False, 1001, []
        found2, ft2 , _ = self.has_car(fname2)
        if found2 == False:
            return False, 1002, []
        dif = np.mean(np.abs(ft1 - ft2))
        return dif < 0.25, dif, im1
    
    def temporal_diff(self, tstamp1, tstamp2, tstamps):
        tstamps = np.int_(tstamps)
        start =  np.where(tstamps -tstamp1 == 0)[0][0]
        end = np.where(tstamps -tstamp2 == 0)[0][0]
        #%% find the beginning of a stationary car
        diffs, ts1, hasCar, ft1, timerStart, sameCar, im1, im1Out= \
          [],   0,    [],  None,   [] ,      [False] , None, []
        for ts2 in tstamps[start:(end+1)]:
            fname2 = '{}.jpg'.format(ts2)
            th = videoDownloader(ts2, 'data')
            th.start()
            th.join()
            dif, ft2, hasCar2, same = np.inf, None, False, False
            if ts1 > 0:
                hasCar2, ft2, im2 = self.has_car(fname2)
                if hasCar2 and hasCar[-1]:
                    dif = np.mean(np.abs(ft1 - ft2))
            if dif < 0.36:
                same = True
            elif dif < np.inf and np.min([diffs[-1], 
                diffs[-min(2,len(diffs))], diffs[-min(3,len(diffs))]]) < 0.36:
                same = True
            if same:    
                if not sameCar[-1]: # rising edge
                    timerStart += [ts1]
                    im1 = im2
            else:                
                if sameCar[-1]: # falling edge
                    im1Out += [self.dump_result(im1, timerStart[-1], ts1)]
            sameCar += [same]
            diffs += [dif] 
            hasCar += [hasCar2]
            ts1, ft1 = ts2, ft2
        print('no more cars found!')
        return diffs, hasCar, tstamps[start:end+1], im1Out

    def dump_result(self, im1, ts0, ts1):
        dur = int(round((ts1 - ts0)/60.0))
        if dur > 0:
            print('found car at {} parked until {} ({} minutes)'.format(ts0, ts1, dur))
            fnout = 'output/{}-{}min.jpg'.format(ts0, dur)
            imsave(fnout,im1)
            plt.figure()
            plt.imshow(im1)
            return im1 
        return []    
        
carFinder = YOLO()
