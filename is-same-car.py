#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 28 15:39:51 2018

@author: Ethan Cheng
"""

from modules.yolo3 import carFinder
import argparse

#%%
if 0:
    carFinder.same_car('1538076003.jpg', '1538076191.jpg')

if __name__ == "__main__":
    # Define arguments
    parser = argparse.ArgumentParser(description='Detect if two cars are the same.')
    parser.add_argument('fname1', type=str, help="The first filename to be tested.")
    parser.add_argument('fname2', type=str, help="The second filename to be tested.")
    args = parser.parse_args()
    print('Comparing {} and {} ...'.format(args.fname1, args.fname2))
    isSame, dif, _ = carFinder.same_car(args.fname1, args.fname2)
    if dif == 1001:
        print("{} has no car found to be matched".format(args.fname1))
    elif dif == 1002:
        print("{} has no car found to be matched".format(args.fname2))
    else:
        print('diff = {:4.3f}'.format(dif))
        if isSame:
            print('Same car !'.format(dif))
        else:
            print('Different car !! '.format(dif))
