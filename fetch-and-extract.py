# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 23:20:55 2018

@author: Ethan Cheng
"""
#%%
import argparse
import matplotlib.pyplot as plt
from modules.yolo3 import videoDownloader
            
#%%                
if 0:
    nTh = 14
    with open('data/index.txt','r') as f:
        lines = f.readlines()
    ts = []
    for line in lines:
        ts += [line.split('.')[0]]
        #%%
    for i in range(0, len(ts),nTh):
        ths = []
        for ts_i in ts[i:i+nTh]:
            ths += [videoDownloader(ts_i, 'data')]
            ths[-1].start()
        for th in ths:
            th.join()

#%%
if __name__ == "__main__":
    # Define arguments
    parser = argparse.ArgumentParser(description='Fetch a verkata video stream and extract the first frame')
    parser.add_argument("timestamps", type=str, help="The timestamp of the video file to be donwload and extracted")
    parser.add_argument('-o', '--outDir', type= str, default = 'data', help = 'Output folder.')
    args = parser.parse_args()
    videoDownloader(args.timestamps, args.outDir).run()

        
