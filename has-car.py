#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 12:19:48 2018

@author: Ethan Cheng
"""
from modules.yolo3 import carFinder
import argparse

#%% sandbox area for testing
if 0:
    hasCar, ft, im = carFinder.has_car('1538076211.jpg', True)
    carFinder.has_car('1538076175.jpg', True)
    carFinder.has_car('1538076179.jpg', True)
    carFinder.has_car('1538076183.jpg', True)

#%%
if __name__ == "__main__":
    # Define arguments
    parser = argparse.ArgumentParser(description='Detect if a car is found inside the target parking slot.')
    parser.add_argument('fname', type=str, help="The name of the file to be tested.")
    args = parser.parse_args()
    print('MSG: Running Yolo3 on {} ...'.format(args.fname))
    hasCar, ft, im = carFinder.has_car(args.fname)     
    if hasCar:
        print('Found a car !')
    elif ft == []:
        print('Error: data/{} does not exist..'.format(args.fname))
    else: 
        print('No car detected !!')