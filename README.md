# Smart Parking Meters

[//]: # (Image References)

[image0]: ./examples/sample.png "designated parking spot"
[image1]: ./output/1538076175-28min.jpg "car#1"
[image2]: ./output/1538077958-4min.jpg "car#2"
[image3]: ./examples/real_time_matching_error.png "real-time matching error"

---

## Objectives

To detect whether a car is parked in a designated slot, marked by the red rectangle in the picture below. If so, monitor the parking period and report the event with an image named by the timestamp and the duration.

   ![alt text][image0]

The task can be described as an image recognition pipeline consisted of the following stages.

 1. **Fetch and Extract**
     * Request video streaming from a web server and extract the first frame for processing.

 2. **Car Detection**
     * Detect car appearance in the designated zone.

 3. **Car Matching**
     * Check if two cars are the same in the same designated zone in different time stamps.

 4. **Paking Timer**
     * Detect the appearance/disappearance of a stationary car in the designated zone and report the duration after a car leaves the parking slot.

---

## How to Run This Project

 1. Clone the respository to also get the necessary folders created for [intermediate files](./data) and [output](./output) images. A list of MPEG2 video files on the server is available in [index.txt](./index.txt).

 2. Run the following command to detect cars parked at the spot between two time stamps.

```
     python3 analyze-cars.py --index index.txt --start 1538076003 --end 1538078234

     found car at 1538076175. parked until 1538077874 (28 minutes).
     ... wrote output/1538076175-28min.jpg
     found car at 1538077954. parked until 1538078198 (4 minutes).
     ... wrote output/1538077954-4min.jpg
     no more cars found

```

 3. Alternatively, you can run individual components in the pipeline as the following.

     * Fetch/extract an image

         "./fetch-and-extract 1538076003"

     * Detect a car in the desginated spot

         "./has-car 1538076003.jpg"

     * Compare cars from two images

         "./is-same-car 1538076179.jpg 1538076183.jpg"

---

## The Modules

The main functioanlities are implemented in three files in the [modules](./modules) folder.

 1. **VideoDownloader**

     * VideoDownloader implemented in [yolo3.py](./modules/yolo3.py) is a subclass of the  "Thread" class, which requests a video stream through URL and extract the first frame as a JPEG file once done.
     * The thread subclass implementation is aimed to enable parallel downloading as a quick way to improve the pipeline throughput.

 2. **YOLO v3 Object Detector**
     * This module implements the wrapper of a [YOLO version 3](https://pjreddie.com/darknet/yolo/) Darknet, a well documented deep network as a speedy object detector.
     * This module is inspired by the [work on github by qqwwee](https://github.com/qqwweee/keras-yolo3) which provides the tool to convert a pre-trained yolo weight file into Keras H5 format as an encapsulated model.
     * The wrapper can choose to take an Yolo-tiny or a regular network through the H5 files. Both weight files were converted in the folder. Due to the performance issue, the default warpper takes the full standard Yolo network.
     * This wrapper also encapsulates custom recognition methods for this task including car detection, same-car matching and parking duration tracking.

 3. **yolo_model.py**
     * This module contains the utility functions to convert Yolo network outputs into a recognition format in terms of bounding boxes, matching score, and class labels. Note that this file is implemented by [github user qqwweee](https://github.com/qqwweee/keras-yolo3), which I integrated into this pipeline.

---

## Algorithm Details

1. **The car detector**
     * Car detection is done through Yolo's pretrained capability. Once an input is normalized and fed to Yolo. A bunch of bounding boxes are spit out to provide potential candidates for this task.
     * Filtering Yolo's bounding boxes gives a simple car detector for our purpose. All we need to do is looping through each Yolo's bounding box, _i.e. 20 max_, and check if it meets the following criteria:

         * The bounding box is labeled as a car.
         * The bouding box has a significant overlap with the target zone, _i.e. 75%_.
         * The center of the bouding box locates near the center of the target zone.

     * Note that the last filter above is added for robustness as it removes false positives such as cars driving by.

2. **The car matcher**
     * The bottoneck features
         * The car matcher uses Yolo's intermediate network output. In particular, I am extracting the features entering the last convolutional layer. As shown in the partial network summary below, the bottleneck features fit my bills are labeled as,
             * **leaky_re_lu_58**
             * **leaky_re_lu_65**
             * **leaky_re_lu_75**.

     ```
     ________________________________________________________________________________
     ________________________________________________________________________________ 
     ________________________________________________________________________________
     conv2d_58 (Conv2D)           (None, None, None, 1 4718592 leaky_re_lu_57
     ________________________________________________________________________________
     conv2d_66 (Conv2D)           (None, None, None, 5 1179648 leaky_re_lu_64
     ________________________________________________________________________________
     conv2d_74 (Conv2D)           (None, None, None, 2 294912  leaky_re_lu_71
     ________________________________________________________________________________
     batch_normalization_58       (None, None, None, 1 4096    conv2d_58
     ________________________________________________________________________________
     batch_normalization_65       (None, None, None, 5 2048    conv2d_66
     ________________________________________________________________________________
     batch_normalization_72       (None, None, None, 2 1024    conv2d_74
     ________________________________________________________________________________
     leaky_re_lu_58 (LeakyReLU)   (None, None, None, 1 0       batch_normalization_58
     ________________________________________________________________________________
     leaky_re_lu_65 (LeakyReLU)   (None, None, None, 5 0       batch_normalization_65
     ________________________________________________________________________________
     leaky_re_lu_72 (LeakyReLU)   (None, None, None, 2 0       batch_normalization_72
     ________________________________________________________________________________
     conv2d_59 (Conv2D)           (None, None, None, 2 261375  leaky_re_lu_58
     ________________________________________________________________________________
     conv2d_67 (Conv2D)           (None, None, None, 2 130815  leaky_re_lu_65
     ________________________________________________________________________________
     conv2d_75 (Conv2D)           (None, None, None, 2 65535   leaky_re_lu_72
     ================================================================================
     Total params: 62,001,757
     Trainable params: 61,949,149
     Non-trainable params: 52,608
     ________________________________________________________________________________
     ```

     * Note that the feature space is still huge at this layer, which consists of 3 seperated convolutional pipelines. Fortunately, not all of the features are relevant to the task as we are only interested in cars parked in the designated zone. As we know the convolutional layers preserve spatial properties, we can find the relevent pixels in this layer by their scaling factors with respect to the original image size. By simple cropping, the feature space is down to 26624.
     * **The matching**
         * Matching is perfromed by an _average absolute distance_ between two features, done by the following python command.

             * matching diff = np.means(np.abs(ft1 - ft2), axis = 0)

     * **Thresholding**
         * The error reflects a similarity measure which can become a recognizer with a thresholding. For quick implementation, I ran a few cases and pick **0.25** as the threhsold for this task. For a temporary solution, the threshold seems to work decently.

3. The parking timer
     * The parking timer is the final product that uses the entire pipeline described above, plus the dectection of starting/finishing time of a stationary vehicle. This function is implemented as "temporal_diff" method in the YOLO wrapper class.
     * **Real-time matching**
         * To monitor a stationary car in the zone, images between two consecutive frames are matched as a way to achieve lowest recognition errors. As the outdoor lighting slowly changes during daytime, high matching frequency ensures lowest feature discrepency, thus leads to better recognition accuracy.
     * **The false negative issue in real-time detection**
         * Although matching between consecutive frames eases false negatives, false negative events are still inevitable. As the parking duration extends, the number of matching also increases. This issue is illustrated in the figure below. As shown, most of the matching errors are below 0.25 along with many random spikes.

         * A single false negative such as sensor noise breaks **one** parking period into **two**, is likely to happen.
         * To avoid random false negative events, a temporal filter is required. The rational is that the time gap between two frames is too short for it to happen.

     * **The temporal filter**

         * A minimal filter over the past three time stamps, a span of 8 seconds, is added to the pipeline. The filter works to pick the minimum in the error buffer (size =3)as a mechanism to suppress random spikes in the figure above.

         * However, false negative still happens though at a much lower frequency. To further suppress the false negatives, the matching threshold is elevated to 0.3.

            ![alt text][image3]

## Results

 Two identified vehicles and their duration of parking.

* 1538076175-28min.jpg

    ![alt text][image1]

* 1538077958-4min.jpg

    ![alt text][image2]

---

## Discussion on Bonus Optional Challenges

Due to time constraints, I am not able to implement the optional challenges. I would like to present some ideas for discussion.

1. Regarding concurrency flag
     * The thread implementation of the video downloader is aimed to implement the parallel download feature. However, a few experimental downloads show the URL requests are often refused approaching the middle of the index file. It seems some download limits were imposed at the server.  As a result, the implementation is scrapped but can be reverted should the download limit is lifted.
2. Regarding the nearby parking spots
     * The parking spots are fully visible when the street is clear. We can then use the image for parking slot detections. Below I summarize a pipeline to detect parking slots automatically for any street image.
         * Step 1 - image undistortion
             * The captured images show a distortion of fisheyes (wide) lens. The undistortion can be done easily in opencv with a known instrinsic camera matrix.
         * Step 2 - white lane detection
             * With undistorted image as input, an edge detector (i.e., Sobel filters) gives edges of white lanes in the image.
             * As each white lane has edges on both sides, a better white lane representation is to combine both edges into one thin line (1 pixel wide).
         * Step 3 - perspective transformation to birdeye view
             * All 2D cameras capture perspective transformed images of 3D scenes. With some known reference points, we can derive a perspective transformation to generate a birdeye view of the white lanes over the entire street.
         * Step 4 - parking slot detection
             * Parking slots on streets have signature of near 45 degree parallel lanes with respect to the street. As the birdeye view removes perspective distortion, the parallel lanes will be seen to have similar gaps among them, another signature.
         * Step 5 - Reverse perspective transformation
             * Once the parking slots are idetified in the birdeye view, we then map the slots back to the original 2D image through reverse perspective transformation. This will complete the parking slot detection.
3. Are there other algorithms that work better at identifying cars?
     * Descriptors matching will be an alternative to deep learning approach and could be even more effective. The state-of-the-art descriptors are those in binary forms as they are compact and known to be just as effective as non-binary versions (_e.g. SIFT, SURF,..., etc_). Binary descriptors can replace the deep convolutional networks which extract object features. We can then use fully connected layers to perform classifier training on a large database to extract car features from descriptors.

4. Can you detect the color of the car?
     * Color detection in RGB color space is doable but is prone to lighting interference. A better approach is to first convert the image to HSV color space, in which we normalize the lighting channel by histogram equalization.  After that, the image is transformed back to the color image for color detection.

     * Another alternative is to stay in the HSV space but matching the color using only the Hue and Saturation channels.

     * As for those images taken at night, color detection is unlikely as they appear to be black and white images (possibly due to infrared sensing).

---

## Discussion

1. Temporal filter with higher sampling rates

     * The minimal filter as the temporal filter can work better under higher sampling rates. A higher sampling rate reduces the feature discrepancy between two consecutive frames while allowing minimal filter applied on more frames. Both will further suppresses false negatives.

2. Transfer learning for better bottleneck features  

     * Due to the time constraints, the bottleneck features are not fully optimized for the car matching tasks. If sufficient labeled data are available, a transfer learning based on the selected bottleneck features can encode/transform the features to be more effective with fewer dimensions.

3. When do you think this algorithm would work well? when would it not?

     * This algorithm will work well under normal lighting.  Once the lighting drops/jitters, image SNR drops/jitters as well, thus the whole piepline is affected.
     * This algorithm will work well under non-congested street views, namely, the occulsion problem. Assuming the traffic is congested for a period of time, the matching between consecutive frames is expected to fail. Complete understaning of the scene is necessary to deal with occlusion.  

4. What would you suggest for future exploration?

     * Video labeling often demands speed. The descriptor matching described in the section above is an effective alternative as it is cheaper/faster than deep convolutional networks.
     * Object tracking such as Kalman filters would be a good direction to go as well. With tracking, the temporal features such as speed discriminate moving cars from matching with stationaty cars. Object tracking also relax the accuracy burden of same-car matcher using temporal features.
